import { API, get, post, upload } from './request'

class LoginService {

  // 登录 邮箱作为用户名登录
  login(email: string, password: string) {
    return post(`${API}/login`, { email, password });
  }


  // 获取用户名片夹里名片列表
  cardList(params: any) {
    return get(`${API}/api/apps-third-login/card-list`, params)
  }

  // 文件上传接口
  modifyUpload(params: any) {
    return upload(`${API}/api/apps-third-login/upload`, params)
  }

}


const loginService = new LoginService()

export default loginService