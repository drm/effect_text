import React, { useEffect, useState, Fragment } from "react";
import styles from "./index.module.less";
import { autorun } from "mobx";
import clsx from "clsx";
import A1 from './img/a1.png';
import A2 from './img/a2.png';
import A3 from './img/a3.png';
import { CloseOutlined } from "@ant-design/icons";

const houseList = [
    { name: '左键旋转视角', src: A1 },
    { name: '滚轮缩放视角', src: A2 },
    { name: '右键移动视角', src: A3 },
    // { name: '住院部3', value: 3 },
]

const Tips = () => {
    const [hidden, setHidden] = useState(false);


    useEffect(() => {
        const local = localStorage.getItem('tipsIcon');
        if (local && local === 'hidden') {
            setHidden(true);
        }
    }, [])

    const close = () => {
        localStorage.setItem('tipsIcon', 'hidden');
        setHidden(true);
    }

    return (
        <Fragment>
            {
                hidden ? null :
                    <div className={clsx(styles.bottomSlider1)}>
                        <div className={styles.close} onClick={close}>
                            <CloseOutlined style={{ color: '#909399', fontSize: 16, cursor: 'pointer' }} />
                        </div>
                        {
                            houseList.map(({ name, src }, i) => (
                                <div key={i}
                                    className={styles.item}>
                                    <div className={styles.top}><img src={src} /></div>
                                    <div>{name}</div>
                                </div>
                            ))
                        }
                    </div>
            }
        </Fragment>
    )
}
export default Tips;