import { debounce } from "lodash";
import { useState, ReactChild, useRef, useEffect } from "react"
import styles from "./index.module.less";
import moment from 'moment';

type Iprops = {
    width?: number;
    height?: number;
    children?: ReactChild | ReactChild[];
    pointerEvents?: any;
}

function AdaptiveLayout({ width = 1920, height = 1080, children, pointerEvents = "none" }: Iprops) {
    const refDiv = useRef<HTMLDivElement>(null);
    const cWidth = useRef(0)
    const cHeight = useRef(0)
    const originalWidth = useRef(0)
    const originalHeight = useRef(0)
    const [proportion, setProportion] = useState(1);
    const [isFullScreen, setIsFullScreen] = useState(true);
    const [yearMonth, setYearMonth] = useState('');

    const initSize = () => {
        return new Promise((resolve: any) => {
            const dom = refDiv.current;
            if (width && height) {
                cWidth.current = width;
                cHeight.current = height;
            } else {
                cWidth.current = dom!.clientWidth;
                cHeight.current = dom!.clientHeight;
            }
            if (!originalWidth.current || !originalHeight.current) {
                originalWidth.current = window.screen.width;
                originalHeight.current = window.screen.height;
            }
            resolve();
        })

    }

    const updateScale = () => {
        if (refDiv.current) {
            let peripheralWidth = refDiv.current!.offsetWidth;
            setProportion(peripheralWidth / 1920);
        }

    }

    const updateSize = () => {
        const dom = refDiv.current;
        if (cWidth.current && cHeight.current) {
            dom!.style.width = `100%`;
            dom!.style.height = `100%`;
        } else {
            dom!.style.width = `100%`;
            dom!.style.height = `100%`;
        }
    }

    const onResize = async () => {
        await initSize();
        updateScale();
    }

    const init = async () => {
        await initSize();
        // updateSize();
        updateScale();
    }

    useEffect(() => {
        init();
        window.addEventListener("resize", debounce(onResize, 1000));

        setYearMonth(moment().format('YYYY年MM月DD日 HH:mm:ss'));

        const timer = setInterval(() => {
            setYearMonth(moment().format('YYYY年MM月DD日 HH:mm:ss'));
        }, 1000);

        return () => {
            clearInterval(timer);
            window.removeEventListener("resize", onResize);
        }
    }, [])

    // 全屏
    const handleRequestFullScreen = (domElement: any) => {
        if (domElement.requestFullscreen) {
            domElement.requestFullscreen();
        } else if (domElement.webkitRequestFullScreen) {
            domElement.webkitRequestFullScreen();
        } else if (domElement.mozRequestFullScreen) {
            domElement.mozRequestFullScreen();
        } else {
            domElement.msRequestFullscreen();
        }
    };
    // 退出全屏
    const handleExitFullscreen = () => {
        const document: any = window.document;
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExiFullscreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    };

    const handleFullScreen = () => {
        if (isFullScreen) {
            // const peripheral = this.$refs["fixed_peripheral"];

            const peripheral = document.getElementsByTagName('body')[0]
            if (peripheral) {
                // document.getElementsByClassName("fixed_peripheral")[0].classList.add("fullScreenLocation")
                handleRequestFullScreen(peripheral);
            }
        } else {
            // document.getElementsByClassName("fixed_peripheral")[0].classList.remove("fullScreenLocation")
            handleExitFullscreen();
        }
        setIsFullScreen(!isFullScreen);
    };

    return (
        <div className={styles.outBox} ref={refDiv}>
            <div className={styles.AdaptiveLayout} style={{ transform: `scale(${proportion})` }}>
                {children}
                <div className={styles.year}>
                    {yearMonth}
                </div>
                <div className={styles.fullScrenn} onClick={handleFullScreen}>

                </div>
            </div>
        </div>

    )
}

export default AdaptiveLayout;