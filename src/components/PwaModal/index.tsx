import React, { useEffect, useState, Fragment } from "react";
import { autorun } from "mobx";
import clsx from "clsx";
import styles from "./index.module.less";
import { Button, Space } from "antd";


const PwaModal = () => {
    const [show, setShow] = useState(false);


    useEffect(() => {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.addEventListener("controllerchange", () => {
                setShow(true)
            });
        }


    }, [])

    return (
        <div className={clsx(styles.pwaModal, show && styles.pwaModalShow)}>
            <div className={styles.title}>更新完成,请刷新页面</div>
            <div className={styles.btnGroup}>
                <Space align="center" wrap>
                    <Button
                        onClick={() => {
                            setShow(false)
                        }}
                    >不了</Button>
                    <Button type="primary" onClick={() => {
                        window.location.reload()
                    }}>刷新</Button>
                </Space>
            </div>
        </div>
    )
}
export default PwaModal;