
// 声明模块
declare module "*.shp";
declare module "*.dbf";
declare module "get-browser-info";
declare module "dat.gui";
declare module "webm2mp4-js";

declare const _DEV_: boolean;

declare interface Window {
    Root: ReactDOM.Root,
    globalStore: any;
    box3dRender: any;
    carousel3dRenderer: any;
    textBallRender: any;
    hrlRender: any;
}