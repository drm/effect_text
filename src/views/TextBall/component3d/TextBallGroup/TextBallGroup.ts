/*
 * @Author: xiaosihan 
 * @Date: 2023-03-17 21:29:45 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-17 23:19:48
 */

import { DoubleSide, Group, Mesh, MeshBasicMaterial, PlaneGeometry } from "three";
import { SpriteText } from "three-base";
import { degToRad } from "three/src/math/MathUtils";

// 立方体对象
export default class TextBallGroup extends Group {

    constructor() {
        super();

        this.setText("HelloTotem");
    }

    planeGeometry = (() => {
        const planeGeometry = new PlaneGeometry(2.5, 2.5, 1, 1);
        planeGeometry.rotateY(degToRad(180));
        return planeGeometry;
    })();

    planeTextGroup = (() => {
        const planeTextGroup = new Group();
        let r = 17;

        [1, 5, 9, 13, 17, 21, 25, 21, 17, 13, 9, 5, 1].map((num, i) => {

            let theta = Math.PI / 12
            let phi = 2 * Math.PI / num;
            for (var j = 0; j < num; j++) {

                let x = r * Math.sin(theta * i) * Math.sin(phi * j);
                let y = -r * Math.cos(theta * i);
                let z = r * Math.sin(theta * i) * Math.cos(phi * j);

                const planeMesh = new Mesh(
                    this.planeGeometry,
                    new MeshBasicMaterial({
                        transparent: true,
                        side: DoubleSide
                    })
                );
                planeMesh.position.set(x, y, z);
                planeMesh.lookAt(this.position);
                planeTextGroup.add(planeMesh);

            }

        });

        this.add(planeTextGroup);
        return planeTextGroup;
    })();

    // 设置文字
    setText(text = "HelloTotem", color = "#fa7036") {
        if (text) {
            this.planeTextGroup.children.map((obj, i) => {
                const spriteText = new SpriteText(text[i % text.length]);
                spriteText.color = color;
                spriteText.material.map!.repeat.x = spriteText.scale.y / spriteText.scale.x;
                ((obj as Mesh).material as MeshBasicMaterial).map = spriteText.material.map;
            });
        }
    }



}


