/*
 * @Author: xiaosihan 
 * @Date: 2022-08-20 14:50:15 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 05:10:52
 */
import { Vector3, Fog } from "three";
import ThreeBase from "three-base";
import TextBallGroup from "./component3d/TextBallGroup/TextBallGroup";

class TextBallRender extends ThreeBase {

    constructor() {
        super();
    }

    resize(width: number, height: number) {
        super.resize(width, height);
        this.renderer.setPixelRatio(1);
    }

    init() {
        super.init();
        this.resetLookAt({
            center: new Vector3(0, 0, 0),
            position: new Vector3(0, 0, 50)
        });
        this.controls.enablePan = false;
        this.controls.maxPolarAngle = Math.PI / 2;
        this.controls.enableZoom = false;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = -3;

        this.scene.fog = new Fog("#000000", 50, 70);

    }

    // 创建一个转盘
    textBallGroup = (() => {
        const textBallGroup = new TextBallGroup();
        this.scene.add(textBallGroup);
        return textBallGroup;
    })();

    // 是否在预览中
    previewing = false;

    //预览开始时间
    previewStartTime = new Date().valueOf();

    // 预览canvas
    preViewCanvas = (() => {
        const preViewCanvas = document.createElement("canvas");
        preViewCanvas.width = 448;
        preViewCanvas.height = 448;
        return preViewCanvas;
    })();

    // 预览旋转
    async preview() {
        this.resetLookAt({
            center: new Vector3(0, 0, 0),
            position: new Vector3(0, 15, 20)
        });
        this.previewStartTime = new Date().valueOf();
        this.previewing = true;
        while (this.previewing) {
            await new Promise(resolve => setTimeout(resolve, 50));
        }
    }

    downloadVideo = async () => {
        let bolb: Blob | undefined = undefined;
        const allChunks: Array<Blob> = [];
        const ctx = this.preViewCanvas.getContext("2d")!;
        const afterRender = () => {
            ctx.clearRect(0, 0, 448, 448);
            ctx.drawImage(this.renderer.domElement, 0, 0, this.size.x, this.size.y, 0, 0, 448, 448);
        }

        this.addEventListener("afterRender", afterRender);

        const stream = this.preViewCanvas.captureStream(60); // 60 FPS recording

        let recorder = new MediaRecorder(stream, {
            mimeType: 'video/webm;codecs=vp9'
        });
        // canvas 录制回调
        recorder.ondataavailable = (e: { data: Blob; }) => {
            allChunks.push(e.data);
        }
        recorder.onstop = () => {
            bolb = new Blob(allChunks, { type: 'video/mp4' });
            this.removeEventListener("afterRender", afterRender);
        }
        recorder.start(50);
        await this.preview();
        recorder.stop();
        await new Promise(resolve => setTimeout(resolve, 200));
        return bolb;
    }

    render() {

            // 判断角度旋转到 0 之后为1圈
            if (this.previewing) {
                const angle = this.camera.position.clone().setY(0).angleTo(new Vector3(0, 0, 20));
                console.log(angle);
    
                if ((new Date().valueOf() - this.previewStartTime > 1000) && angle < 0.001) {
                    this.previewing = false;
                }
            }
            this.controls.enableDamping = !this.previewing;

        super.render();
    }



}


const textBallRender = window.textBallRender = new TextBallRender();

export default textBallRender;