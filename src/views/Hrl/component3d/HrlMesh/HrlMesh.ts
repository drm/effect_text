/*
 * @Author: xiaosihan 
 * @Date: 2023-03-17 21:29:45 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-17 23:19:48
 */

import { DoubleSide, Group, Mesh, MeshBasicMaterial, PlaneGeometry, MeshStandardMaterial } from "three";
import { SpriteText } from "three-base";
import { degToRad } from "three/src/math/MathUtils";
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry.js';
import fontContorller from "./fontController";



// 立方体对象
export default class HrlMesh extends Mesh {

    constructor() {
        super();

        this.setText("HelloTotem");
    }

    material = new MeshStandardMaterial({
        metalness: 0.8, // 金属度，值为0到1
        roughness: 0.5, // 粗糙度，值为0到1
    });

    text = "";

    // 设置文字
    setText(text = "HelloTotem", color = "#fa7036") {
        this.material.color.set(color);

        if (text && text !== this.text) {
            this.text = text;
            fontContorller.loadText({
                text: text,
                callback: THREEFont => {
                    this.geometry = new TextGeometry(text, {
                        font: THREEFont,
                        size: 10,
                        curveSegments: 10,
                        height: 1, // 文字的厚度
                        bevelEnabled: true, // 斜角开启
                        bevelThickness: 0.2, // 斜角厚度
                        bevelSize: 0.15, // 斜角大小
                        bevelOffset: 0,// 斜角偏移
                        bevelSegments: 3,//斜角分段
                    });
                    this.geometry.center();
                }
            })
        }
    }



}


