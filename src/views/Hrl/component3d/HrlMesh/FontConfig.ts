/*
 * @Author: xiaosihan
 * @Date: 2021-04-24 19:13:41
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-10-24 22:19:20
 */



//字体默认的配置
const FontConfig = {
    // 完整文字路径的展示
    fontPathDemo: "http://drm.gitee.io/three-base-font/simhei/字.js",
    path: "http://drm.gitee.io/three-base-font/",
    path0: "字体的访问路径配置",
}

export default FontConfig;

