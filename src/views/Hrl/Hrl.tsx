/*
 * @Author: xiaosihan 
 * @Date: 2022-07-11 07:51:56 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 05:59:29
 */
import hashHistory from "@hashHistory";
import { Button } from "antd";
import { Popup, TextArea, TextAreaRef } from "antd-mobile";
import { LeftOutline } from 'antd-mobile-icons';
import { useEffect, useRef, useState } from "react";
import { CirclePicker } from 'react-color';
import styles from './hrl.module.less';
import hrlRender from "./hrlRender";

export default function Hrl() {

    const [color, setColor] = useState("#fa7036");
    const [visible, setVisible] = useState(false);
    const textArea = useRef<TextAreaRef | null>(null);

    useEffect(() => {

    }, []);

    const ok = () => {
        setVisible(false);
        if (textArea.current && textArea.current.nativeElement) {
            hrlRender.hrlMesh.setText(textArea.current.nativeElement.value || "HelloTotem", color);
        }
    }

    return (
        <div className={styles.hrl}>

            <div ref={dom => hrlRender.setContainer(dom)} className={styles.container}></div>

            <div className={styles.inputtext} onClick={() => setVisible(true)} >click to enter text</div>

            <Popup
                bodyClassName={styles.popup}
                visible={visible}
                onMaskClick={() => {
                    setVisible(false)
                }}
                bodyStyle={{ height: 300 }}
            >
                <div className={styles.textArea} >
                    <TextArea ref={textArea} placeholder='HelloTotem' rows={5} />
                </div>

                <CirclePicker
                    color={color}
                    colors={["#ffffff", "#fa7036", "#fcb900", "#7bdcb5", "#00d084", "#8ed1fc", "#abb8c3", "#f78da7"]}
                    onChangeComplete={color => {
                        setColor(color.hex);
                    }}
                />

                <Button.Group className={styles.buttonGroup} >
                    <Button size="large" ghost className={styles.btn} onClick={() => ok()} >ok</Button>
                    <Button size="large" ghost className={styles.btn} onClick={() => setVisible(false)}>cancal</Button>
                </Button.Group>

            </Popup>

        </div>
    );
}