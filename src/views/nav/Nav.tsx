/*
 * @Author: xiaosihan 
 * @Date: 2023-03-17 23:37:03 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 05:55:22
 */


import { List } from "antd-mobile";
import { AaOutline, AppOutline, AppstoreOutline, GlobalOutline } from 'antd-mobile-icons';
import { useRef, useState } from "react";
import styles from "./nav.module.less";

interface Iprops {

}


/**
 *导航
 */
export default function Nav(props: Iprops) {

    const dom = useRef<HTMLDivElement | null>(null);
    const [data, setData] = useState({});

    return (
        <div className={styles.nav} ref={dom} >
            <List header='catalogue'>
                <List.Item prefix={<AppstoreOutline />} onClick={() => { location.href = "/3d_box.html" }}> 3d box</List.Item>
                <List.Item prefix={<AppOutline />} onClick={() => { location.href = "/3d_carousel.html" }}>3d carousel</List.Item>
                <List.Item prefix={<AaOutline />} onClick={() => { location.href = "/3d_hrl.html" }}  >3d hrl</List.Item>
                <List.Item prefix={<GlobalOutline />} onClick={() => { location.href = "/3d_text_ball.html" }}>3d text ball</List.Item>
            </List>
        </div >
    );

}