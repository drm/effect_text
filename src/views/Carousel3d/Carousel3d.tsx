/*
 * @Author: xiaosihan 
 * @Date: 2022-07-11 07:51:56 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 05:58:55
 */
import hashHistory from "@hashHistory";
import { Button } from "antd";
import { Popup, TextArea, TextAreaRef } from "antd-mobile";
import { LeftOutline } from 'antd-mobile-icons';
import { useEffect, useRef, useState } from "react";
import { CirclePicker } from 'react-color';
import styles from './carousel3d.module.less';
import carousel3dRenderer from "./carousel3dRender";


export default function Carousel3d() {

    const [img0, setImg0] = useState("");
    const [img1, setImg1] = useState("");
    const [img2, setImg2] = useState("");
    const [img3, setImg3] = useState("");
    const [img4, setImg4] = useState("");
    const [img5, setImg5] = useState("");

    const [color, setColor] = useState("#ffffff");
    const [visible, setVisible] = useState(false);
    const textArea = useRef<TextAreaRef | null>(null);

    const changeMap = async (index: 0 | 1 | 2 | 3 | 4 | 5) => {

        const blob = await carousel3dRenderer.carousel.changeMap(index);

        switch (index) {
            case 0: setImg0(blob); break;
            case 1: setImg1(blob); break;
            case 2: setImg2(blob); break;
            case 3: setImg3(blob); break;
            case 4: setImg4(blob); break;
            case 5: setImg5(blob); break;

            default:
                break;
        }

    }

    const setText = () => {
        setVisible(false);
        if (textArea.current && textArea.current.nativeElement) {
            carousel3dRenderer.carousel.setText(textArea.current.nativeElement.value || "HelloTotem", color);
        }
    }

    useEffect(() => {
        carousel3dRenderer.carousel.initAnimation();
    }, []);

    return (
        <div className={styles.carousel3d}>

            <div ref={dom => carousel3dRenderer.setContainer(dom)} className={styles.container}></div>

            <div className={styles.imgs} >
                {
                    img0 ?
                        <img className={styles.img} src={img0} onClick={() => changeMap(0)} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap(0)} >image1</button>
                }
                {
                    img1 ?
                        <img className={styles.img} src={img1} onClick={() => changeMap(1)} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap(1)} >image2</button>
                }

                {
                    img2 ?
                        <img className={styles.img} src={img2} onClick={() => changeMap(2)} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap(2)} >image3</button>
                }
            </div>

            <div className={styles.imgs} >
                {
                    img3 ?
                        <img className={styles.img} src={img3} onClick={() => changeMap(3)} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap(3)} >image4</button>
                }
                {
                    img4 ?
                        <img className={styles.img} src={img4} onClick={() => changeMap(4)} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap(4)} >image5</button>
                }

                {
                    img5 ?
                        <img className={styles.img} src={img5} onClick={() => changeMap(5)} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap(5)} >image6</button>
                }
            </div>

            <div className={styles.inputtext} onClick={() => setVisible(true)} >click to enter text</div>

            <Popup
                bodyClassName={styles.popup}
                visible={visible}
                onMaskClick={() => {
                    setVisible(false)
                }}
                bodyStyle={{ height: 300 }}
            >
                <div className={styles.textArea} >
                    <TextArea ref={textArea} placeholder='HelloTotem' rows={5} />
                </div>

                <CirclePicker
                    color={color}
                    colors={["#ffffff", "#ff6900", "#fcb900", "#7bdcb5", "#00d084", "#8ed1fc", "#abb8c3", "#f78da7"]}
                    onChangeComplete={color => {
                        setColor(color.hex);
                    }}
                />

                <Button.Group className={styles.buttonGroup} >
                    <Button size="large" ghost className={styles.btn} onClick={() => setText()} >ok</Button>
                    <Button size="large" ghost className={styles.btn} onClick={() => setVisible(false)}  >cancel</Button>
                </Button.Group>

            </Popup>

        </div>
    );
}