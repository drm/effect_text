/*
 * @Author: xiaosihan 
 * @Date: 2023-03-18 21:31:52 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 03:53:50
 */



//mesh
import { BufferGeometry, Mesh, ShaderChunk, ShaderMaterial, DoubleSide } from "three";
import { threeLoader } from "three-base";

// 倒影模型
export default class Inverted extends Mesh {
    constructor(bufferGeometry: BufferGeometry) {
        super(bufferGeometry);
        this.scale.set(1, -1, 1);
    }

    // 设置贴图
    setMap(src: string) {
        this.material.uniforms.map.value = threeLoader.getTexture(src);
    }

    material = new ShaderMaterial({
        uniforms: {
            map: { value: null }, // 贴图纹理
        },
        vertexShader: `
        ${ShaderChunk.common}
        ${ShaderChunk.logdepthbuf_pars_vertex}
        varying vec2 vUv;
        void main() {
            vUv = uv;
            // gl_Position = vec4(position, 1.0);
            // gl_Position = projectionMatrix * modelViewMatrix * instanceMatrix * vec4(position, 1.0);
            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            ${ShaderChunk.logdepthbuf_vertex}
        }
    `,
        fragmentShader: `
        ${ShaderChunk.logdepthbuf_pars_fragment}
        uniform sampler2D map;
        varying vec2 vUv;
        void main() {
            gl_FragColor = texture2D(map, vUv);
            gl_FragColor.a = 0.5 - vUv.y;
            ${ShaderChunk.logdepthbuf_fragment}
        }
    `,
        depthTest: true,
        depthWrite: true,
        transparent: true,
        side: DoubleSide
    });


}