/*
 * @Author: xiaosihan 
 * @Date: 2022-08-20 14:50:15 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 05:24:16
 */
import { Vector3 } from "three";
import ThreeBase from "three-base";
import Carousel from "./component3d/Carousel/Carousel";

class Carousel3dRenderer extends ThreeBase {

    constructor() {
        super();
    }

    resize(width: number, height: number) {
        super.resize(width, height);
        this.renderer.setPixelRatio(1);
    }

    init() {
        super.init();
        this.resetLookAt({
            center: new Vector3(0, 5, 0),
            position: new Vector3(0, 25, 50)
        });
        this.controls.enablePan = false;
        this.controls.maxPolarAngle = Math.PI / 2;
        this.controls.enableZoom = false;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = -3;
    }

    // 创建一个转盘
    carousel = (() => {
        const carousel = new Carousel();
        this.scene.add(carousel);
        return carousel;
    })();


    // 是否在预览中
    previewing = false;

       // 预览canvas
       preViewCanvas = (() => {
        const preViewCanvas = document.createElement("canvas");
        preViewCanvas.width = 448;
        preViewCanvas.height = 448;
        return preViewCanvas;
    })();

    // 预览旋转
    async preview() {
        this.resetLookAt({
            center: new Vector3(0, 0, 0),
            position: new Vector3(0, 15, 20)
        });
        await new Promise(resolve => setTimeout(resolve, 2000));

        this.previewing = true;
        while (this.previewing) {
            await new Promise(resolve => setTimeout(resolve, 50));
        }
    }

    downloadVideo = async () => {
        let bolb: Blob | undefined = undefined;
        const allChunks: Array<Blob> = [];

        const ctx = this.preViewCanvas.getContext("2d")!;

        const afterRender = () => {
            ctx.clearRect(0, 0, 448, 448);
            ctx.drawImage(this.renderer.domElement, 0, 0, this.size.x, this.size.y , 0, 0, 448, 448);
        }

        this.addEventListener("afterRender", afterRender);

        const stream = this.preViewCanvas.captureStream(60); // 60 FPS recording

        let recorder = new MediaRecorder(stream, {
            mimeType: 'video/webm;codecs=vp9'
        });
        // canvas 录制回调
        recorder.ondataavailable = (e: { data: Blob; }) => {
            allChunks.push(e.data);
        }
        recorder.onstop = () => {
            bolb = new Blob(allChunks, { type: 'video/mp4' });
            this.removeEventListener("afterRender", afterRender);
        }
        recorder.start(50);
        await this.preview();
        recorder.stop();
        await new Promise(resolve => setTimeout(resolve, 200));
        return bolb;
    }



    render() {

        // 判断角度旋转到 0 之后为1圈
        if (this.previewing) {
            const angle = this.camera.position.clone().setY(0).angleTo(new Vector3(0, 0, 20));
            if (angle === 0) {
                this.previewing = false;
            }
        }

        super.render();
    }

}


const carousel3dRenderer = window.carousel3dRenderer = new Carousel3dRenderer();

export default carousel3dRenderer;