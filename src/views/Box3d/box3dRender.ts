/*
 * @Author: xiaosihan 
 * @Date: 2022-08-20 14:50:15 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-29 00:34:12
 */
import { Vector3 } from "three";
import ThreeBase from "three-base";
import Box from "./component3d/Box/Box";

class Box3dRender extends ThreeBase {

    constructor() {
        super();
    }

    resize(width: number, height: number) {
        super.resize(width, height);
        this.renderer.setPixelRatio(1);
    }

    init() {
        super.init();
        this.resetLookAt({
            center: new Vector3(0, 0, 0),
            position: new Vector3(0, 15, 20)
        });
        this.controls.enablePan = false;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = -50;
    }

    // 创建一个立方体
    box = (() => {
        const box = new Box();
        this.scene.add(box);
        return box;
    })();

    // 是否在预览中
    previewing = false;

    //预览开始时间
    previewStartTime = new Date().valueOf();

    // 预览canvas
    preViewCanvas = (() => {
        const preViewCanvas = document.createElement("canvas");
        preViewCanvas.width = 448;
        preViewCanvas.height = 448;
        return preViewCanvas;
    })();

    // 预览旋转
    async preview() {
        this.resetLookAt({
            center: new Vector3(0, 0, 0),
            position: new Vector3(0, 15, 20)
        });
        this.previewStartTime = new Date().valueOf();
        this.previewing = true;
        while (this.previewing) {
            await new Promise(resolve => setTimeout(resolve, 50));
        }
    }

    downloadVideo = async () => {
        let bolb: Blob | undefined = undefined;
        const allChunks: Array<Blob> = [];
        const ctx = this.preViewCanvas.getContext("2d")!;
        const afterRender = () => {
            ctx.clearRect(0, 0, 448, 448);
            ctx.drawImage(this.renderer.domElement, 0, 0, this.size.x, this.size.y , 0, 0, 448, 448);
        }

        this.addEventListener("afterRender", afterRender);

        const stream = this.preViewCanvas.captureStream(60); // 60 FPS recording

        let recorder = new MediaRecorder(stream, {
            mimeType: 'video/webm;codecs=vp9'
        });
        // canvas 录制回调
        recorder.ondataavailable = (e: { data: Blob; }) => {
            allChunks.push(e.data);
        }
        recorder.onstop = () => {
            bolb = new Blob(allChunks, { type: 'video/mp4' });
            this.addEventListener("afterRender", afterRender);
        }
        recorder.start(50);
        await this.preview();
        recorder.stop();
        await new Promise(resolve => setTimeout(resolve, 200));
        return bolb;
    }

    render() {

        // 判断角度旋转到 0 之后为1圈
        if (this.previewing) {
            const angle = this.camera.position.clone().setY(0).angleTo(new Vector3(0, 0, 20));
            console.log(angle);

            if ((new Date().valueOf() - this.previewStartTime > 1000) && angle < 0.001) {
                this.previewing = false;
            }
        }
        this.controls.enableDamping = !this.previewing;

        super.render();
    }



}


const box3dRender = window.box3dRender = new Box3dRender();

export default box3dRender;