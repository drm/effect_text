/*
 * @Author: xiaosihan 
 * @Date: 2022-07-11 07:51:56 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 05:56:49
 */

import { Radio, RadioChangeEvent } from "antd";
import { useEffect, useState } from "react";
import styles from './box3d.module.less';
import box3dRender from "./box3dRender";

export default function Box3d() {

    const [effect, setEffect] = useState<"effect1" | "effect2" | "effect3">("effect1");
    const [visible, setVisible] = useState(false);
    const [frontImg, setFrontImg] = useState("");
    const [backImg, setBackImg] = useState("");
    const [leftImg, setLeftImg] = useState("");
    const [rightImg, setRightImg] = useState("");
    const [topImg, setTopImg] = useState("");
    const [bottomImg, setBottomImg] = useState("");

    useEffect(() => {
        box3dRender.box.setEffect(effect);
    }, []);


    const changeMap = async (direction: 'front' | 'back' | 'left' | 'right' | 'top' | 'bottom') => {

        const blob = await box3dRender.box.changeMap(direction);

        switch (direction) {
            case "front":
                setFrontImg(blob);
                break;
            case "back":
                setBackImg(blob);
                break;
            case "left":
                setLeftImg(blob);
                break;
            case "right":
                setRightImg(blob);
                break;
            case "top":
                setTopImg(blob);
                break;
            case "bottom":
                setBottomImg(blob);
                break;

            default:
                break;
        }

    }

    return (
        <div className={styles.box3d}>

            <div ref={dom => box3dRender.setContainer(dom)} className={styles.container}></div>

            <video id="video" src="" muted autoPlay loop ></video>

            <div className={styles.btns} >

                <Radio.Group
                    options={[
                        { label: 'effect1', value: 'effect1', },
                        { label: 'effect2', value: 'effect2', },
                        { label: 'effect3', value: 'effect3', },
                    ]}
                    value={effect}
                    optionType="button"
                    buttonStyle="solid"
                    onChange={({ target: { value } }: RadioChangeEvent) => {
                        setEffect(value);
                        box3dRender.box.setEffect(value);
                    }}
                />
            </div>

            <div className={styles.imgs} >
                {
                    frontImg ?
                        <img className={styles.img} src={frontImg} onClick={() => changeMap("front")} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap("front")} >image1</button>
                }
                {
                    backImg ?
                        <img className={styles.img} src={backImg} onClick={() => changeMap("back")} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap("back")} >image2</button>
                }

                {
                    leftImg ?
                        <img className={styles.img} src={leftImg} onClick={() => changeMap("left")} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap("left")} >image3</button>
                }
            </div>

            <div className={styles.imgs} >
                {
                    rightImg ?
                        <img className={styles.img} src={rightImg} onClick={() => changeMap("right")} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap("right")} >image4</button>
                }
                {
                    topImg ?
                        <img className={styles.img} src={topImg} onClick={() => changeMap("top")} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap("top")} >image5</button>
                }

                {
                    bottomImg ?
                        <img className={styles.img} src={bottomImg} onClick={() => changeMap("bottom")} alt="" /> :
                        <button className={styles.btn} onClick={() => changeMap("bottom")} >image6</button>
                }
            </div>

        </div>
    );
}


