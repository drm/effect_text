/*
 * @Author: xiaosihan 
 * @Date: 2023-03-17 21:29:45 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-17 23:19:48
 */

import { BufferGeometry, Group, Mesh, MeshBasicMaterial, PlaneGeometry, DoubleSide } from "three";
import { degToRad } from "three/src/math/MathUtils";
import { threeLoader, Transition } from "three-base";
import mapJPG from "./map.jpg";
import utils from "@utils";



// 立方体对象
export default class Box extends Group {

    constructor() {
        super();
    }

    planeGeometry = new PlaneGeometry(10, 10, 1, 1);

    // 上面的立方体
    topPlane = (() => {
        const topPlane = new Mesh<BufferGeometry, MeshBasicMaterial>(
            this.planeGeometry,
            new MeshBasicMaterial({ transparent: true, opacity: 0.8, side: DoubleSide, map: threeLoader.getTexture(mapJPG) })
        );
        topPlane.position.set(0, 5, 0);
        topPlane.rotateX(degToRad(-90));
        this.add(topPlane);
        return topPlane;
    })();

    // 下面的立方体
    bottomPlane = (() => {
        const bottomPlane = new Mesh<BufferGeometry, MeshBasicMaterial>(
            this.planeGeometry,
            new MeshBasicMaterial({ transparent: true, opacity: 0.8, side: DoubleSide, map: threeLoader.getTexture(mapJPG) })
        );
        bottomPlane.position.set(0, -5, 0);
        bottomPlane.rotateX(degToRad(90));
        this.add(bottomPlane);
        return bottomPlane;
    })();

    // 左面的立方体
    leftPlane = (() => {
        const leftPlane = new Mesh<BufferGeometry, MeshBasicMaterial>(
            this.planeGeometry,
            new MeshBasicMaterial({ transparent: true, opacity: 0.8, side: DoubleSide, map: threeLoader.getTexture(mapJPG) })
        );
        leftPlane.position.set(-5, 0, 0);
        leftPlane.rotateY(degToRad(-90));
        this.add(leftPlane);
        return leftPlane;
    })();

    // 右面的立方体
    rightPlane = (() => {
        const rightPlane = new Mesh<BufferGeometry, MeshBasicMaterial>(
            this.planeGeometry,
            new MeshBasicMaterial({ transparent: true, opacity: 0.8, side: DoubleSide, map: threeLoader.getTexture(mapJPG) })
        );
        rightPlane.position.set(5, 0, 0);
        rightPlane.rotateY(degToRad(90));
        this.add(rightPlane);
        return rightPlane;
    })();

    // 前面的立方体
    frontPlane = (() => {
        const frontPlane = new Mesh<BufferGeometry, MeshBasicMaterial>(
            this.planeGeometry,
            new MeshBasicMaterial({ transparent: true, opacity: 0.8, side: DoubleSide, map: threeLoader.getTexture(mapJPG) })
        );
        frontPlane.position.set(0, 0, 5);
        this.add(frontPlane);
        return frontPlane;
    })();

    // 后面的立方体
    backPlane = (() => {
        const backPlane = new Mesh<BufferGeometry, MeshBasicMaterial>(
            this.planeGeometry,
            new MeshBasicMaterial({ transparent: true, opacity: 0.8, side: DoubleSide, map: threeLoader.getTexture(mapJPG) })
        );
        backPlane.position.set(0, 0, -5);
        backPlane.rotateY(degToRad(180));
        this.add(backPlane);
        return backPlane;
    })();


    // 特效变换对象
    transition = (() => {
        const transition = new Transition({ progress: 0 }).setDuration(2000).setBezier([0, 0, 1, 1]);
        transition.onChange(({ progress }) => {
            this.frontPlane.position.setZ(5 + (progress * 1.5));
            this.backPlane.position.setZ(-5 - (progress * 1.5));
            this.leftPlane.position.setX(-5 - (progress * 1.5));
            this.rightPlane.position.setX(5 + (progress * 1.5));
            this.topPlane.position.setY(5 + (progress * 1.5));
            this.bottomPlane.position.setY(-5 - (progress * 1.5));
        });
        return transition;
    })();

    //当前特效类型
    currentEffect: "effect1" | "effect2" | "effect3" = "effect1";

    // 动效3需要循环
    isLoop = false;

    timer = 0;

    // 设置特效
    setEffect(effect: "effect1" | "effect2" | "effect3") {
        clearTimeout(this.timer);
        this.currentEffect = effect;
        switch (effect) {
            case "effect1":
                this.transition.reset({ progress: 0 });
                break;
            case "effect2":
                this.transition.reset({ progress: 1 });
                break;
            case "effect3":
                this.transition.reset({ progress: 0 }).set({ progress: 1 });
                this.timer = setTimeout(() => {
                    if (this.currentEffect === "effect3") {
                        this.setEffect("effect3");
                    }
                }, 15000);
                break;
            default:
                break;
        }
    }

    // 换图
    async changeMap(direction: 'front' | 'back' | 'left' | 'right' | 'top' | 'bottom') {

        const blob = await utils.selectFileToBlob();

        if (blob) {
            switch (direction) {
                case "front":
                    this.frontPlane.material.map = threeLoader.getTexture(blob);
                    break;
                case "back":
                    this.backPlane.material.map = threeLoader.getTexture(blob);
                    break;
                case "left":
                    this.leftPlane.material.map = threeLoader.getTexture(blob);
                    break;
                case "right":
                    this.rightPlane.material.map = threeLoader.getTexture(blob);
                    break;
                case "top":
                    this.topPlane.material.map = threeLoader.getTexture(blob);
                    break;
                case "bottom":
                    this.bottomPlane.material.map = threeLoader.getTexture(blob);
                    break;

                default:
                    break;
            }
        }

        return blob || "";

    }


}