/*
 * @Author: xiaosihan
 * @Date: 2021-03-28 02:13:06
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-17 22:37:50
 */

import { get } from "lodash";
import { Clock, Event, EventDispatcher } from "three";

// 全局公共方法
class Utils extends EventDispatcher {
    constructor() {
        super();
    }

    isProd = import.meta.env.MODE as string === "production";

    isDev = import.meta.env.MODE as string === "development";

    // 数字加千分位
    thousand(number: string | number, dot = 0) {
        let strNum = String(Number(Number(number).toFixed(dot))); // 转化成字符串
        while (strNum !== strNum.replace(/(\d)(\d{3})(\.|,|$)/, "$1,$2$3")) {
            strNum = strNum.replace(/(\d)(\d{3})(\.|,|$)/, "$1,$2$3");
        }
        return strNum;
    }

    // 数字转换单位 默认保留2位小数
    numToUnit(num: number, dot = 2) {
        if (Math.abs(num) >= 1000000000000) {
            return { value: this.thousand(num / 1000000000000, dot), unit: "万亿" };
        } else if (Math.abs(num) >= 100000000) {
            return { value: this.thousand(num / 100000000, dot), unit: "亿" };
        } else if (Math.abs(num) >= 10000) {
            return { value: this.thousand(num / 10000, dot), unit: "万" };
        } else {
            return { value: this.thousand(num, dot), unit: "" };
        }
    }

    // 取随机数在某个范围里
    rand(start: number, end: number) {
        return Math.floor(Math.random() * (end - start + 1) + start);
    }

    // 获取是星期几
    getWeekDay() {
        const d = new Date().getDay();
        const arr = ["天", "一", "二", "三", "四", "五", "六"];
        return arr[d];
    }

    // 获取日期
    getDate(seperator: string = ".") {
        // 获取当前日期
        let date = new Date();

        // 获取当前月份
        let nowMonth = date.getMonth() + 1;

        // 获取当前是几号
        let strDate = date.getDate();


        // 对月份进行处理，1-9月在前面添加一个“0”
        if (nowMonth >= 1 && nowMonth <= 9) {
            nowMonth = "0" + nowMonth as any;
        }

        // 对月份进行处理，1-9号在前面添加一个“0”
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate as any;
        }

        // 最后拼接字符串，得到一个格式为(yyyy-MM-dd)的日期
        let nowDate = date.getFullYear() + seperator + nowMonth + seperator + strDate;
        return nowDate;
    }

    // 获取时分秒
    getHourMinuteSecond(seperator: string = ":") {
        let today = new Date();
        let h = today.getHours();
        let m = today.getMinutes();
        let s = today.getSeconds();
        // 在 numbers<10 的数字前加上 0
        m = m < 10 ? "0" + m : m as any;
        s = s < 10 ? "0" + s : s as any;
        return h + seperator + m + seperator + s;
    }

    // 选择文件
    async selectFile() {

        return new Promise<File>((resolve, reject) => {
            const inputDom = document.createElement("input");
            inputDom.style.display = "none";
            inputDom.type = "file";
            inputDom.onchange = (e) => {
                const file = get(e, ["target", "files", 0]);
                resolve(file);
            }
            document.body.append(inputDom);
            inputDom.click();
        });

    }

    //app返回的信息
    receiveData(data: string) {
        console.log('app receive', data)
        let msg = JSON.parse(data);
        //webview 返回base64
        console.log('app receive', msg)

        this.dispatchEvent({ type: "receive_data", msg });
    }

    async selectFileToBlob(accept = ".jpg,.jpeg,.png,.tga", multiple = false) {

        //请求app唤起相册
        //@ts-ignore
        uni.postMessage({
            data: {
                album: true
            }
        });

        const msg = await new Promise((resove, reject) => {
            const receiveDataMsg = ({ msg }: any) => {
                this.removeEventListener("receive_data", receiveDataMsg);
                resove(msg);
            }
            this.addEventListener("receive_data", receiveDataMsg);
        });

        return msg;
    }

    // 时钟对象
    clock = new Clock();

    // 判断当前运行是否流畅
    isFluency() {

    }



}

const utils = new Utils();

export default utils;