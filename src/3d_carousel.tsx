/*
 * @Author: xiaosihan 
 * @Date: 2022-08-20 17:08:13 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-03-19 05:31:44
 */
import 'antd-mobile/es/global';
import ReactDOM from 'react-dom/client';
import './index.css';
import Carousel3d from '@views/Carousel3d/Carousel3d';
// import VConsole from 'vconsole';
// new VConsole();

const Root = window.Root || (window.Root = ReactDOM.createRoot(document.getElementById('root')!));

Root.render(<Carousel3d />);

//@ts-ignore
console.log("前端发版时间", process.env.BUILD_TIME);
console.log("程序问题反馈联系方式 %cqq389652405", 'color: red');

// 程序更新提示
// if ('serviceWorker' in navigator) {
//     navigator.serviceWorker.addEventListener("controllerchange", () => {
//         console.log("更新完成");
//         Modal.confirm({
//             type: "success",
//             centered: true,
//             title: "更新完成,请刷新页面",
//             okText: "刷新",
//             cancelText: "不了",
//             onOk: () => window.location.reload()
//         })
//     });
// }